<?php

/**
 * This is the model base class for the table "classificacao".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Classificacao".
 *
 * Columns in table "classificacao" available as properties of the model,
 * followed by relations of table "classificacao" available as properties of the model.
 *
 * @property integer $id
 * @property string $codigo
 * @property integer $classe
 * @property integer $subclasse
 * @property integer $grupo
 * @property integer $subgrupo
 * @property integer $unidade_arquivamento
 *
 * @property Classe $idClasse
 * @property Subclasse $idSubclasse
 * @property Grupo $idGrupo
 * @property Subgrupo $idSubgrupo
 * @property UnidadeArquivamento $idUnidadeArquivamento
 * @property ItemDocumental[] $itensDocumentais
 */
abstract class BaseClassificacao extends GxActiveRecord {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function tableName() {
        return 'classificacao';
    }

    public static function label($n = 1) {
        return Yii::t('app', 'Nível de Classificação|Níveis de Classificações', $n);
    }

    public static function representingColumn() {
        return 'codigo';
    }

    public function rules() {
        return array(
            array('classe', 'required'),
            array('classe, subclasse, grupo, subgrupo, unidade_arquivamento', 'numerical', 'integerOnly' => true),
            array('codigo', 'length', 'max' => 29),
            array('subclasse, grupo, subgrupo, unidade_arquivamento', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, codigo, classe, subclasse, grupo, subgrupo, unidade_arquivamento', 'safe', 'on' => 'search'),
        );
    }

    public function relations() {
        return array(
            'idClasse' => array(self::BELONGS_TO, 'Classe', 'classe'),
            'idSubclasse' => array(self::BELONGS_TO, 'Subclasse', 'subclasse'),
            'idGrupo' => array(self::BELONGS_TO, 'Grupo', 'grupo'),
            'idSubgrupo' => array(self::BELONGS_TO, 'Subgrupo', 'subgrupo'),
            'idUnidadeArquivamento' => array(self::BELONGS_TO, 'UnidadeArquivamento', 'unidade_arquivamento'),
            'itensDocumentais' => array(self::HAS_MANY, 'ItemDocumental', 'classificacao'),
        );
    }

    public function pivotModels() {
        return array(
        );
    }

    public function attributeLabels() {
        return array(
            'id' => Yii::t('app', 'ID'),
            'codigo' => Yii::t('app', 'Codigo'),
            'classe' => null,
            'subclasse' => null,
            'grupo' => null,
            'subgrupo' => null,
            'unidade_arquivamento' => null,
            'classe0' => null,
            'subclasse0' => null,
            'grupo0' => null,
            'subgrupo0' => null,
            'unidadeArquivamento' => null,
            'itensDocumentais' => null,
        );
    }

    public function search() {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('codigo', $this->codigo, true);
        $criteria->compare('classe', $this->classe);
        $criteria->compare('subclasse', $this->subclasse);
        $criteria->compare('grupo', $this->grupo);
        $criteria->compare('subgrupo', $this->subgrupo);
        $criteria->compare('unidade_arquivamento', $this->unidade_arquivamento);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function beforeSave() {
        if (parent::beforeSave()) {
            $codArray = array();
            if ($this->idClasse->codigo != null)
                $codArray[] = $this->idClasse->codigo;
            
            if (isset($this->idSubclasse) && $this->idSubclasse->codigo != '')
                $codArray[] = $this->idSubclasse->codigo;

            if (isset($this->idGrupo) && $this->idGrupo->codigo != '')
                $codArray[] = $this->idGrupo->codigo;

            if (isset($this->idSubgrupo) && $this->idSubgrupo->codigo != '')
                $codArray[] = $this->idSubgrupo->codigo;

            if (isset($this->idUnidadeArquivamento) && $this->idUnidadeArquivamento->codigo != '')
                $codArray[] = $this->idUnidadeArquivamento->codigo;

            $this->codigo = implode('-', $codArray);
            
            if ($this->isNewRecord) {
                $condition = 'classe = ' . $this->classe;
                
                if (isset($this->subclasse) && $this->subclasse != '')
                    $condition .= ' AND subclasse = ' . $this->subclasse;
                
                if (isset($this->grupo) && $this->grupo != '')
                    $condition .= ' AND grupo = ' . $this->grupo;
                
                if (isset($this->subgrupo) && $this->subgrupo != '')
                    $condition .= ' AND subgrupo = ' . $this->subgrupo;
                
                if (isset($this->unidade_arquivamento) && $this->unidade_arquivamento != '')
                    $condition .= ' AND unidade_arquivamento = ' . $this->unidade_arquivamento;
                
                $model = Classificacao::model()->find(array(
                    'condition' => $condition
                ));
                
                if ($model)
                    return false;
            }
            
            return true;
        }
        return false;    
    }
    
}
    