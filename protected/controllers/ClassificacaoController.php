<?php

class ClassificacaoController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Classificacao'),
        ));
    }

    public function actionCreate() {
        $model = new Classificacao;
        
        if (isset($_POST['Classificacao'])) {
            $model->setAttributes($_POST['Classificacao']);
            
            if (!isset($_POST['Classificacao']['classe']) || $_POST['Classificacao']['classe'] == '')
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/index/error/noclass');
            
            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
            } else {
                $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/index/error/unique');
            }
        } else {
            $this->redirect(Yii::app()->request->getBaseUrl(true) . '/site/index/error/noclass');
        }

        $this->redirect(Yii::app()->request->getBaseUrl(true));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Classificacao');


        if (isset($_POST['Classificacao'])) {
            $model->setAttributes($_POST['Classificacao']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        $model = Classificacao::model()->findByPk($id);
        $error = '';
        
        if ($model) {
            if (isset($model->itensDocumentais) && !empty($model->itensDocumentais)) {
                $error = 'Erro ao deletar Nível de Classificação "' . $model->codigo . '".';
            } else {
                $model->delete();
                $error = 'success';
            }
        }
        echo $error;
    }

    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Classificacao');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    public function actionAdmin() {
        $model = new Classificacao('search');
        $model->unsetAttributes();

        if (isset($_GET['Classificacao']))
            $model->setAttributes($_GET['Classificacao']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }
    
}
