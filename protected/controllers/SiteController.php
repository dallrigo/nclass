<?php

class SiteController extends Controller {

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex($error = '') {
        $nclass = Classificacao::model()->findAll();
        $model = new Classificacao;
        
        $this->render('index', array(
            'nclass' => $nclass,
            'model' => $model,
            'error' => $error,
        ));
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public static function getFullName($id, $classe) {
        switch ($classe) {
            case 'Classe':
                $model = Classe::model()->findByPk($id);
                break;
            case 'Subclasse':
                $model = Subclasse::model()->findByPk($id);
                break;
            case 'Grupo':
                $model = Grupo::model()->findByPk($id);
                break;
            case 'Subgrupo':
                $model = Subgrupo::model()->findByPk($id);
                break;
            case 'UnidadeArquivamento':
                $model = UnidadeArquivamento::model()->findByPk($id);
                break;
            default:
                break;
        }
        
        $str = '';
        if (isset($model) && $model) {
            if (isset($model->codigo) && $model->codigo != '')
                $str .= $model->codigo . ' - ';
            
            $str .= $model->nome;
        }
        
        return $str;
    }
    
}
