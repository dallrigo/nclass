<?php

class SubclasseController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Subclasse'),
        ));
    }

    public function actionCreate() {
        $model = new Subclasse;


        if (isset($_POST['Subclasse'])) {
            $model->setAttributes($_POST['Subclasse']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Subclasse');

            if (isset($model->classificacoes) && !empty($model->classificacoes)) {
                throw new CHttpException(400, Yii::t('app', 'Erro ao deletar. Subclasse pertence a um Nível de Classificação.'));
            } else {
                $model->delete();
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    /*
      public function actionIndex() {
      $dataProvider = new CActiveDataProvider('Subclasse');
      $this->render('index', array(
      'dataProvider' => $dataProvider,
      ));
      }
     */

    public function actionIndex() {
        $model = new Subclasse('search');
        $model->unsetAttributes();

        if (isset($_GET['Subclasse']))
            $model->setAttributes($_GET['Subclasse']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}
