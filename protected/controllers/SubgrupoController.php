<?php

class SubgrupoController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Subgrupo'),
        ));
    }

    public function actionCreate() {
        $model = new Subgrupo;


        if (isset($_POST['Subgrupo'])) {
            $model->setAttributes($_POST['Subgrupo']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Subgrupo');

            if (isset($model->classificacoes) && !empty($model->classificacoes)) {
                throw new CHttpException(400, Yii::t('app', 'Erro ao deletar. Subgrupo pertence a um Nível de Classificação.'));
            } else {
                $model->delete();
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    /*
      public function actionIndex() {
      $dataProvider = new CActiveDataProvider('Subgrupo');
      $this->render('index', array(
      'dataProvider' => $dataProvider,
      ));
      }
     */

    public function actionIndex() {
        $model = new Subgrupo('search');
        $model->unsetAttributes();

        if (isset($_GET['Subgrupo']))
            $model->setAttributes($_GET['Subgrupo']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}
