<?php

class ClasseController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Classe'),
        ));
    }

    public function actionCreate() {
        $model = new Classe;

        if (isset($_POST['Classe'])) {
            $model->setAttributes($_POST['Classe']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Classe');

        if (isset($_POST['Classe'])) {
            $model->setAttributes($_POST['Classe']);
            
            if ($model->save()) {
                if (isset($model->classificacoes) && !empty($model->classificacoes)) {
                    foreach ($model->classificacoes as $nclass) {
                        $codigo = strstr($nclass->codigo, '-');
                        $nclass->codigo = $model->codigo . $codigo;
                        $nclass->save();
                    }
                }

                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Classe');
            
            if (isset($model->classificacoes) && !empty($model->classificacoes))
                throw new CHttpException(400, Yii::t('app', 'Erro ao deletar. Classe pertence a um Nível de Classificação.'));
            else if ($model->ativo == 1)
                throw new CHttpException(400, Yii::t('app', 'Erro ao deletar. Classe deve estar inativa.'));
            else
                $model->delete();
            
            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionDeactivate($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Classe');
            $model->ativo = 0;
            $model->save();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionActivate($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Classe');
            $model->ativo = 1;
            $model->save();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('index'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    public function actionIndex() {
        $model = new Classe('search');
        $model->unsetAttributes();

        if (isset($_GET['Classe']))
            $model->setAttributes($_GET['Classe']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}
