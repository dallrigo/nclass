<?php

class ItemDocumentalController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'ItemDocumental'),
        ));
    }

    public function actionCreate($nclass = 0) {
        $model = new ItemDocumental;

        if ($nclass > 0)
            $model->classificacao = $nclass;

        if (isset($_POST['ItemDocumental'])) {
            $model->setAttributes($_POST['ItemDocumental']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'ItemDocumental');


        if (isset($_POST['ItemDocumental'])) {
            $model->setAttributes($_POST['ItemDocumental']);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $this->loadModel($id, 'ItemDocumental')->delete();

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    /*
      public function actionIndex() {
      $dataProvider = new CActiveDataProvider('ItemDocumental');
      $this->render('index', array(
      'dataProvider' => $dataProvider,
      ));
      }
     */

    public function actionIndex() {
        $model = new ItemDocumental('search');
        $model->unsetAttributes();

        if (isset($_GET['ItemDocumental']))
            $model->setAttributes($_GET['ItemDocumental']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionMover($nclass) {
        $model = $this->loadModel($nclass, 'Classificacao');

        $itens = ItemDocumental::model()->findAll(array(
            'condition' => 'classificacao = ' . $nclass,
        ));

        if (isset($_POST['Mover'])) {
            if (isset($_POST['Mover']['item']['all'])) {
                foreach ($itens as $item) {
                    $item->classificacao = $_POST['Mover']['para'];
                    $item->save();
                }
            } else {
                foreach ($_POST['Mover']['item'] as $key => $item) {
                    $modelItem = ItemDocumental::model()->findByPk($key);
                    $modelItem->classificacao = $_POST['Mover']['para'];
                    $modelItem->save();
                }
            }
            
            $this->redirect(Yii::app()->request->getBaseUrl(true));
        }

        $this->render('_mover', array(
            'model' => $model,
            'itens' => $itens,
        ));
    }

}
