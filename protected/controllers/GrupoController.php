<?php

class GrupoController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Grupo'),
        ));
    }

    public function actionCreate() {
        $model = new Grupo;


        if (isset($_POST['Grupo'])) {
            $model->setAttributes($_POST['Grupo']);
            $model->data = date("Y-m-d H:i:s");
            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'Grupo');
            
            if (isset($model->classificacoes) && !empty($model->classificacoes)) {
                throw new CHttpException(400, Yii::t('app', 'Erro ao deletar. Grupo pertence a um Nível de Classificação.'));
            } else {
                $model->delete();
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }
/*
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Grupo');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }
 * 
 */

    public function actionIndex() {
        $model = new Grupo('search');
        $model->unsetAttributes();

        if (isset($_GET['Grupo']))
            $model->setAttributes($_GET['Grupo']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}
