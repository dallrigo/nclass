<?php

class UnidadeArquivamentoController extends GxController {

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'UnidadeArquivamento'),
        ));
    }

    public function actionCreate() {
        $model = new UnidadeArquivamento;


        if (isset($_POST['UnidadeArquivamento'])) {
            $model->setAttributes($_POST['UnidadeArquivamento']);

            if ($model->save()) {
                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else
                    $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array('model' => $model));
    }

    public function actionDelete($id) {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            $model = $this->loadModel($id, 'UnidadeArquivamento');

            if (isset($model->classificacoes) && !empty($model->classificacoes)) {
                throw new CHttpException(400, Yii::t('app', 'Erro ao deletar. Unidade de Arquivamento pertence a um Nível de Classificação.'));
            } else {
                $model->delete();
            }

            if (!Yii::app()->getRequest()->getIsAjaxRequest())
                $this->redirect(array('admin'));
        } else
            throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
    }

    /*
      public function actionIndex() {
      $dataProvider = new CActiveDataProvider('UnidadeArquivamento');
      $this->render('index', array(
      'dataProvider' => $dataProvider,
      ));
      }
     */

    public function actionIndex() {
        $model = new UnidadeArquivamento('search');
        $model->unsetAttributes();

        if (isset($_GET['UnidadeArquivamento']))
            $model->setAttributes($_GET['UnidadeArquivamento']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

}
