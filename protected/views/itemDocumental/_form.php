<div class="form">


    <?php
    $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'item-documental-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">
        <?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.
    </p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'classificacao'); ?>
        <?php echo $form->dropDownList($model, 'classificacao', GxHtml::listDataEx(Classificacao::model()->findAllAttributes(null, true))); ?>
        <?php echo $form->error($model, 'classificacao'); ?>
    </div><!-- row -->
    
    <div class="row">
        <?php echo $form->labelEx($model, 'codigo'); ?>
        <?php echo $form->textField($model, 'codigo', array('maxlength' => 35)); ?>
        <?php echo $form->error($model, 'codigo'); ?>
    </div><!-- row -->

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Save'));
    $this->endWidget();
    ?>
</div><!-- form -->