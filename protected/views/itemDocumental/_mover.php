<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

<h1>Mover itens</h1>

<div class="form">
    <?php
    $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'mover-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <fieldset>
        <legend>Níveis de Classificação</legend>

        <div class="row-inline">
            <?php echo CHtml::label('De', 'Classificacao_codigo'); ?>
            <?php echo $form->textField($model, 'codigo', array('readonly' => 'readonly')); ?>
        </div>

        <div class="row-inline">
            <?php echo CHtml::label('Para', 'Mover_para'); ?>
            <?php echo CHtml::dropDownList('Mover[para]', '', GxHtml::listDataEx(Classificacao::model()->findAll(array('condition' => 'id != ' . $model->primaryKey))), array('maxlength' => 35, 'id' => 'Mover_para')); ?>
        </div>

    </fieldset>

    <fieldset>
        <legend>Itens que serão movidos:</legend>
        <?php echo CHtml::checkBox('Mover[item][all]', true); ?>
        <label for="Mover_item_all" style="display: inline-block; margin-left: 10px;">Selecionar todos</label>
        <ul class="mover-itens">
            <?php foreach ($itens as $item) { ?>
                <li>
                    <?php echo CHtml::checkBox('Mover[item][' . $item->primaryKey . ']', true); ?>
                    <label for="Mover_item_<?php echo $item->primaryKey; ?>" style="display: inline-block; margin-left: 10px;"><?php echo $item->codigoCompleto; ?></label>
                </li>
            <?php } ?>
        </ul>
    </fieldset>

    <?php
    echo GxHtml::submitButton(Yii::t('app', 'Mover'));
    $this->endWidget();
    ?>
</div>

<script>
    $(document).ready(function() {
        $('#Mover_item_all').on('click', function() {
            var checkboxes = $('.mover-itens').find(':checkbox');
            if ($(this).is(':checked')) {
                checkboxes.attr('checked', 'checked');
            } else {
                checkboxes.removeAttr('checked');
            }
        });

        $('.mover-itens').find(':checkbox').each(function() {
            $(this).bind('click', function() {
                if (!$(this).is(':checked'))
                    $('#Mover_item_all').removeAttr('checked');
                else {
                    var checkboxes = $('.mover-itens').find(':checkbox');
                    var all = true;
                    $(checkboxes).each(function() {
                        if (!$(this).is(':checked')) {
                            all = false;
                            return;
                        }
                    });

                    if (all)
                        $('#Mover_item_all').attr('checked', 'checked');
                    else
                        $('#Mover_item_all').removeAttr('checked');
                }
            });
        });
    });
</script>