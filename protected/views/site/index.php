<h1>Níveis de Classificação</h1>

<?php
Yii::app()->clientScript->registerScript('search', "
    $('.nclass-button').click(function(){
            $('.nclass-form').toggle();
            return false;
    });
    ");
?>

<?php echo GxHtml::link(Yii::t('app', 'Adicionar Nível de Classificação'), '#', array('class' => 'nclass-button')); ?>
<div class="nclass-form" style="display: <?php echo (isset($error) && $error != '') ? 'block' : 'none'; ?>;">
    <?php
    $this->renderPartial('/classificacao/_form', array(
        'model' => $model,
        'error' => $error,
    ));
    ?>
</div>

<div class="nclass-error"></div>

<div id = "nclass">
    <?php if (isset($nclass) && !empty($nclass)) { ?>

        <ul class="label">
            <li style="background: #fff;">&nbsp;</li>
            <li>Classe</li>
            <li>Subclasse</li>
            <li>Grupo</li>
            <li>Subgrupo</li>
            <li>Unidade de Arquivamento</li>
            <li>Itens Documentais</li>
        </ul>

        <?php foreach ($nclass as $class) { ?>

            <ul class="nivel <?php echo $class->idClasse->ativo == 1 ? 'ativo' : 'inativo'; ?>">
                <li class="nclass-delete">
                    <a href="javascript:;" data-id="<?php echo $class->primaryKey; ?>">Excluir</a>
                </li>

                <?php
                echo '<li>' . $this::getFullName($class->classe, get_class($class->idClasse)) . '</li>';

                if (isset($class->idSubclasse) && $class->idSubclasse)
                    echo '<li>' . $this::getFullName($class->subclasse, get_class($class->idSubclasse)) . '</li>';
                else
                    echo '<li class="off">&nbsp;</li>';

                if (isset($class->idGrupo) && $class->idGrupo)
                    echo '<li>' . $this::getFullName($class->grupo, get_class($class->idGrupo)) . '</li>';
                else
                    echo '<li class="off">&nbsp;</li>';

                if (isset($class->idSubgrupo) && $class->idSubgrupo)
                    echo '<li>' . $this::getFullName($class->subgrupo, get_class($class->idSubgrupo)) . '</li>';
                else
                    echo '<li class="off">&nbsp;</li>';

                if (isset($class->idUnidadeArquivamento) && $class->idUnidadeArquivamento)
                    echo '<li>' . $this::getFullName($class->unidade_arquivamento, get_class($class->idUnidadeArquivamento)) . '</li>';
                else
                    echo '<li class="off">&nbsp;</li>';
                ?>
                <li class="itens">
                    <?php if (isset($class->itensDocumentais) && !empty($class->itensDocumentais)) { ?>
                        <ul>
                            <?php foreach ($class->itensDocumentais as $item) { ?>
                                <li>
                                    <a href="<?php echo Yii::app()->request->getBaseUrl(true) . '/itemDocumental/' . $item->primaryKey; ?>"><?php echo $item->codigo; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                        <a href="<?php echo Yii::app()->request->getBaseUrl(true) . '/itemDocumental/mover/nclass/' . $class->primaryKey; ?>">Mover itens</a>
                    <?php } ?>
                    <a href="<?php echo Yii::app()->request->getBaseUrl(true) . '/itemDocumental/create/nclass/' . $class->primaryKey; ?>">Incluir item</a>
                </li>
            </ul>

        <?php } ?>

    <?php } else { ?>
        <div class="no-result">Nenhum resultado encontrado.</div>
    <?php } ?>

</div>
<?php if (isset($nclass) && !empty($nclass)) { ?>
    <div id="legenda" style="display: block;">
        <table>
            <tr>
                <th colspan="2">Legenda</th>
            </tr>
            <tr>
                <td class="ativo"></td>
                <td>Classe Ativa</td>
            </tr>
            <tr>
                <td class="inativo"</td>
                <td>Classe Inativa</td>
            </tr>
        </table>
    </div>
<?php } ?>

<script>
    $(document).ready(function() {
        $('.nclass-delete').find('a').on('click', function() {
            var r = confirm("Você tem certeza disso?");
            if (r == true) {

                $.ajax({
                    url: '<?php echo Yii::app()->request->getBaseUrl(true) . '/classificacao/delete/'; ?>' + $(this).data('id'),
                    data: '',
                    success: function(data) {
                        console.log(data);

                        if (data == 'success')
                            window.location.reload();
                        else {
                            $('.nclass-error').html(data);
                        }
                    }
                });


            } else
                return false;

        });
    });
</script>