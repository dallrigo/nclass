<?php
$this->breadcrumbs = array(
    $model->label(2) => array('index'),
    GxHtml::valueEx($model) => array('view', 'id' => GxActiveRecord::extractPkValue($model, true)),
    Yii::t('app', 'Update'),
);

$this->menu = array(
    array('label' => Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('index')),
    array('label' => Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
    array('label' => Yii::t('app', 'View') . ' ' . $model->label(), 'url'=>array('view', 'id' => GxActiveRecord::extractPkValue($model, true))),
);
if ($model->ativo == 1) {
    $this->menu[] = array('label' => Yii::t('app', 'Desativar') . ' ' . $model->label(), 'url' => '#', 'linkOptions' => array('submit' => array('deactivate', 'id' => $model->id), 'confirm' => 'Are you sure you want to deactivate this item?'));
} else {
    $this->menu[] = array('label' => Yii::t('app', 'Ativar') . ' ' . $model->label(), 'url' => '#', 'linkOptions' => array('submit' => array('activate', 'id' => $model->id), 'confirm' => 'Are you sure you want to activate this item?'));
}
?>

<h1><?php echo Yii::t('app', 'Update') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php
$this->renderPartial('_form', array(
    'model' => $model));
?>