<?php

$this->breadcrumbs = array(
	Classe::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Classe::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Classe::label(2), 'url' => array('index')),
);
?>

<h1><?php echo GxHtml::encode(Classe::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 