<?php

$this->breadcrumbs = array(
	Subgrupo::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Subgrupo::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Subgrupo::label(2), 'url' => array('index')),
);
?>

<h1><?php echo GxHtml::encode(Subgrupo::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 