<?php

$this->breadcrumbs = array(
	Grupo::label(2),
	Yii::t('app', 'Index'),
);

$this->menu = array(
	array('label'=>Yii::t('app', 'Create') . ' ' . Grupo::label(), 'url' => array('create')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . Grupo::label(2), 'url' => array('index')),
);
?>

<h1><?php echo GxHtml::encode(Grupo::label(2)); ?></h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 