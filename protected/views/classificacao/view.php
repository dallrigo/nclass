<?php

$this->breadcrumbs = array(
	$model->label(2) => array('index'),
	GxHtml::valueEx($model),
);

$this->menu=array(
	array('label'=>Yii::t('app', 'List') . ' ' . $model->label(2), 'url'=>array('index')),
	array('label'=>Yii::t('app', 'Create') . ' ' . $model->label(), 'url'=>array('create')),
	array('label'=>Yii::t('app', 'Update') . ' ' . $model->label(), 'url'=>array('update', 'id' => $model->id)),
	array('label'=>Yii::t('app', 'Delete') . ' ' . $model->label(), 'url'=>'#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app', 'View') . ' ' . GxHtml::encode($model->label()) . ' ' . GxHtml::encode(GxHtml::valueEx($model)); ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data' => $model,
	'attributes' => array(
'id',
'codigo',
array(
			'name' => 'idClasse',
			'type' => 'raw',
			'value' => $model->idClasse !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idClasse)), array('classe/view', 'id' => GxActiveRecord::extractPkValue($model->idClasse, true))) : null,
			),
array(
			'name' => 'idSubclasse',
			'type' => 'raw',
			'value' => $model->idSubclasse !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idSubclasse)), array('subclasse/view', 'id' => GxActiveRecord::extractPkValue($model->idSubclasse, true))) : null,
			),
array(
			'name' => 'idGrupo',
			'type' => 'raw',
			'value' => $model->idGrupo !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idGrupo)), array('grupo/view', 'id' => GxActiveRecord::extractPkValue($model->idGrupo, true))) : null,
			),
array(
			'name' => 'idSubgrupo',
			'type' => 'raw',
			'value' => $model->idSubgrupo !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idSubgrupo)), array('subgrupo/view', 'id' => GxActiveRecord::extractPkValue($model->idSubgrupo, true))) : null,
			),
array(
			'name' => 'idUnidadeArquivamento',
			'type' => 'raw',
			'value' => $model->idUnidadeArquivamento !== null ? GxHtml::link(GxHtml::encode(GxHtml::valueEx($model->idUnidadeArquivamento)), array('unidadeArquivamento/view', 'id' => GxActiveRecord::extractPkValue($model->idUnidadeArquivamento, true))) : null,
			),
	),
)); ?>

<h2><?php echo GxHtml::encode($model->getRelationLabel('itensDocumentais')); ?></h2>
<?php
	echo GxHtml::openTag('ul');
	foreach($model->itensDocumentais as $relatedModel) {
		echo GxHtml::openTag('li');
		echo GxHtml::link(GxHtml::encode(GxHtml::valueEx($relatedModel)), array('itemDocumental/view', 'id' => GxActiveRecord::extractPkValue($relatedModel, true)));
		echo GxHtml::closeTag('li');
	}
	echo GxHtml::closeTag('ul');
?>