<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'id'); ?>
		<?php echo $form->textField($model, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'codigo'); ?>
		<?php echo $form->textField($model, 'codigo', array('maxlength' => 29)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'classe'); ?>
		<?php echo $form->dropDownList($model, 'classe', GxHtml::listDataEx(Classe::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'subclasse'); ?>
		<?php echo $form->dropDownList($model, 'subclasse', GxHtml::listDataEx(Subclasse::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'grupo'); ?>
		<?php echo $form->dropDownList($model, 'grupo', GxHtml::listDataEx(Grupo::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'subgrupo'); ?>
		<?php echo $form->dropDownList($model, 'subgrupo', GxHtml::listDataEx(Subgrupo::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'unidade_arquivamento'); ?>
		<?php echo $form->dropDownList($model, 'unidade_arquivamento', GxHtml::listDataEx(UnidadeArquivamento::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
