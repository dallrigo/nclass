<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('codigo')); ?>:
	<?php echo GxHtml::encode($data->codigo); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('classe')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idClasse)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('subclasse')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idSubclasse)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('grupo')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idGrupo)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('subgrupo')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idSubgrupo)); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('unidade_arquivamento')); ?>:
		<?php echo GxHtml::encode(GxHtml::valueEx($data->idUnidadeArquivamento)); ?>
	<br />

</div>