<div class="form">

    <?php
    $form = $this->beginWidget('GxActiveForm', array(
        'id' => 'classificacao-form',
        'enableAjaxValidation' => false,
        'action' => Yii::app()->request->getBaseUrl(true) . '/classificacao/create'
    ));
    ?>

    <fieldset>
        <div class="row-inline">
            <?php echo $form->labelEx($model, 'classe'); ?>
            <?php echo $form->dropDownList($model, 'classe', GxHtml::listDataEx(Classe::model()->findAllAttributes(null, true))); ?>
            <?php echo $form->error($model, 'classe'); ?>
        </div><!-- row -->
        <div class="row-inline">
            <?php echo $form->labelEx($model, 'subclasse'); ?>
            <?php echo $form->dropDownList($model, 'subclasse', GxHtml::listDataEx(Subclasse::model()->findAllAttributes(null, true)), array('empty' => '')); ?>
            <?php echo $form->error($model, 'subclasse'); ?>
        </div><!-- row -->
        <div class="row-inline">
            <?php echo $form->labelEx($model, 'grupo'); ?>
            <?php echo $form->dropDownList($model, 'grupo', GxHtml::listDataEx(Grupo::model()->findAllAttributes(null, true)), array('empty' => '')); ?>
            <?php echo $form->error($model, 'grupo'); ?>
        </div><!-- row -->
        <div class="row-inline">
            <?php echo $form->labelEx($model, 'subgrupo'); ?>
            <?php echo $form->dropDownList($model, 'subgrupo', GxHtml::listDataEx(Subgrupo::model()->findAllAttributes(null, true)), array('empty' => '')); ?>
            <?php echo $form->error($model, 'subgrupo'); ?>
        </div><!-- row -->
        <div class="row-inline">
            <?php echo $form->labelEx($model, 'unidade_arquivamento'); ?>
            <?php echo $form->dropDownList($model, 'unidade_arquivamento', GxHtml::listDataEx(UnidadeArquivamento::model()->findAllAttributes(null, true)), array('empty' => '')); ?>
            <?php echo $form->error($model, 'unidade_arquivamento'); ?>
        </div><!-- row -->

        <?php
        echo GxHtml::submitButton(Yii::t('app', 'Save'));
        $this->endWidget();
        ?>

        <?php if (isset($error) && $error != '') { ?>
            <div class="error" style="display: inline-block; color: red; margin-left: 10px;">
                <?php
                switch ($error) {
                    case 'unique':
                        echo 'Nível de Classificação já existe.';
                        break;
                    case 'noclass':
                        echo 'Classe é obrigatório.';
                        break;
                    default:
                        break;
                }
                ?>
            </div>
        <?php } ?>

    </fieldset>
</div><!-- form -->