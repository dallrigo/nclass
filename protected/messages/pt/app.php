<?php
return array(
    # CRUD
    'Create' => 'Criar',
    'Manage' => 'Gerenciar',
    'List' => 'Listar',
    'Update' => 'Atualizar',
    'Fields with' => 'Campos com',
    'are required' => 'são obrigatórios',
    'Save' => 'Salvar',
    'All' => 'Todos',
    'Search' => 'Buscar',
    'Advanced Search' => 'Busca avançada',
    'Index' => 'Index',
    'View' => 'Visualizar',
    'Edit' => 'Atualizar',
    'Belongs to' => 'Pertence a',
    'Delete' => 'Excluir',
    'Add' => 'Adicionar',
)
    
?>