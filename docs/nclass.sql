-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2014 at 12:21 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nclass`
--

-- --------------------------------------------------------

--
-- Table structure for table `anterior`
--

CREATE TABLE IF NOT EXISTS `anterior` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `data` datetime NOT NULL,
  `classe` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_anterior_classe1_idx` (`classe`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classe`
--

CREATE TABLE IF NOT EXISTS `classe` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `data` datetime NOT NULL,
  `ativo` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `classificacao`
--

CREATE TABLE IF NOT EXISTS `classificacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(29) NOT NULL,
  `classe` int(11) NOT NULL,
  `subclasse` int(11) DEFAULT NULL,
  `grupo` int(11) DEFAULT NULL,
  `subgrupo` int(11) DEFAULT NULL,
  `unidade_arquivamento` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_classificacao_classe_idx` (`classe`),
  KEY `fk_classificacao_subclasse1_idx` (`subclasse`),
  KEY `fk_classificacao_grupo1_idx` (`grupo`),
  KEY `fk_classificacao_subgrupo1_idx` (`subgrupo`),
  KEY `fk_classificacao_unidade_arquivamento1_idx` (`unidade_arquivamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE IF NOT EXISTS `grupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `item_documental`
--

CREATE TABLE IF NOT EXISTS `item_documental` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(35) NOT NULL,
  `data` datetime NOT NULL,
  `classificacao` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_item_documental_classificacao1_idx` (`classificacao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subclasse`
--

CREATE TABLE IF NOT EXISTS `subclasse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subgrupo`
--

CREATE TABLE IF NOT EXISTS `subgrupo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `unidade_arquivamento`
--

CREATE TABLE IF NOT EXISTS `unidade_arquivamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(120) NOT NULL,
  `codigo` varchar(5) DEFAULT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nome_UNIQUE` (`nome`),
  UNIQUE KEY `codigo_UNIQUE` (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `anterior`
--
ALTER TABLE `anterior`
  ADD CONSTRAINT `fk_anterior_classe1` FOREIGN KEY (`classe`) REFERENCES `classe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `classificacao`
--
ALTER TABLE `classificacao`
  ADD CONSTRAINT `fk_classificacao_classe` FOREIGN KEY (`classe`) REFERENCES `classe` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_classificacao_grupo1` FOREIGN KEY (`grupo`) REFERENCES `grupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_classificacao_subclasse1` FOREIGN KEY (`subclasse`) REFERENCES `subclasse` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_classificacao_subgrupo1` FOREIGN KEY (`subgrupo`) REFERENCES `subgrupo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_classificacao_unidade_arquivamento1` FOREIGN KEY (`unidade_arquivamento`) REFERENCES `unidade_arquivamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `item_documental`
--
ALTER TABLE `item_documental`
  ADD CONSTRAINT `fk_item_documental_classificacao1` FOREIGN KEY (`classificacao`) REFERENCES `classificacao` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
